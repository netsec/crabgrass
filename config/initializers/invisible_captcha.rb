InvisibleCaptcha.setup do |config|
  config.timestamp_enabled = !Rails.env.test?
end

ActiveSupport::Notifications.subscribe('invisible_captcha.spam_detected') do |*args, data|
  Rails.logger.warn 'Potential spam detected. Signup refused.'
end
